package show;

# This file is part of sqlsus
#
# Copyright (c) 2008-2011 Jérémy Ruffet (sativouf)
# http://sqlsus.sourceforge.net/
#
# Licensed under GPLv3+: GNU GPL version 3 or later
# See LICENSE file or http://www.gnu.org/licenses/gpl.html

use strict;
use warnings;

##################################
sub show_database {
	my $db = shift;
	$db = $main::database unless $db;

	print "\n<( $db )>\n";
	my @tabcounts = &db::query("SELECT DISTINCT table_name,count FROM databases WHERE database_name = ? AND table_name != ''", $db);
	while (my $table = shift @tabcounts) {
		my $count = shift @tabcounts;
		if (defined $count and $count ne "") {
			print "\n\t[$table : $count]\n";
		} else {
			print "\n\t[$table]\n";
		}
		for my $col (&db::query("SELECT column_name FROM databases WHERE database_name = ? and table_name = ?", $db, $table)) {
			print "\t\t$col\n";
		}
	}
}

##################################
sub command {
	my $args = shift;
	my (@header, @contents);

	for ($args) {
		if (/^(tables|columns|(db|database)$)/) {
			if (not defined $main::database or not $main::database) {
				&msg::error("Target database not defined, use \"start\" first");
				return 0;
			}
		}
		if (/^target$/i) {
			@header = ("Variable", "Value");
			@contents = map { $_, $main::target{$_} } sort keys %main::target ;
		} elsif (/^priv(?:ilege)?s(?:\s+(.*))?$/i) {
			if (not defined $1 or $1 =~ /^(user|)$/i) {
				@header = ("GRANTEE", "PRIVILEGE_TYPE");
				@contents = &db::query("SELECT grantee,privilege_type FROM privileges WHERE grantee = ?", $main::target{user});
			}
		} elsif (/^(?:database|db)(?:\s+(.*))?$/i) {
			&show_database($1);
			return 1;
		} elsif (/^databases$/i) {
			@header = ("Databases");
			@contents = &db::query("SELECT DISTINCT database_name FROM databases");
		} elsif (/^tables$/i) {
			@header = ("Tables in $main::database");
			@contents = &db::query("SELECT DISTINCT table_name FROM databases WHERE database_name = ?", $main::database);
			if (not @contents) {
				&msg::error("No tables found for the current database. You may want to run \"get tables\" first");
				return 0;
			}
		} elsif (/^columns (.*)$/i) {
			my $table = $1;
			if (not &db::query("SELECT table_name FROM databases WHERE database_name = ? and table_name = ?", $main::database, $table)) {
				&msg::error("No such table found for the current database. You may want to run \"get tables\" first");
				return 0;
			}
			@header = ("Columns in $table");
			@contents = &db::query("SELECT column_name FROM databases WHERE database_name = ? and table_name = ?", $main::database, $table);
		} elsif (/^all$/i) {
			for my $arg (("target", "privileges user", "databases")) {
				my $padding = int((30 - length($arg)) / 2);
				&msg::raw("="x$padding . "[ $arg ]" . "="x$padding . "\n");
				&command($arg);
			}
			return 1;
		} else { 
			&help::usage("show"); 
			return 0;
		}
	}
	if (not @contents) {
		&msg::info("Nothing has been fetched (yet) for that item\n");
		return 0;
	}
	&functions::print_array_in_a_box(undef, \@header, @contents);
	return 1;
}

1
