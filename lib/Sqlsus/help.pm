package help;

# This file is part of sqlsus
#
# Copyright (c) 2008-2011 Jérémy Ruffet (sativouf)
# http://sqlsus.sourceforge.net/
#
# Licensed under GPLv3+: GNU GPL version 3 or later
# See LICENSE file or http://www.gnu.org/licenses/gpl.html

use strict;
use warnings;

use Switch;

sub usage {
	my $command = shift;

	print STDERR "\n";
	switch ($command) {
		case "help" { print STDERR <<EOF;
Usage : help [command]

This command will display help information [on the given command].
EOF
		}
		case "!" { print STDERR <<EOF;
Usage : ! <command>

Execute the specified command on your local system.
EOF
		}
		case "start" { print STDERR <<EOF;
Usage : start

This is the entry point of all further sqlsus magic :)
In inband mode, it will find the columns for UNION
In blind or inband, it will fill \%target (cf config file : \%target_keys), and autoconf max_sendable
EOF
		} 
		case "get" { print STDERR <<EOF;
Usage : get <item>

This command retrieves information from MySQL system tables (information_schema.*).
Note that it will only work on MySQL >= 5. Otherwise, see "brute".

Possible items :
tables               : get the names of the tables of the current database (as per "set")
columns [table_name] : get the names of the columns [for the given table]
count [table_name]   : get the count(*) for each table [or the given table]
db                   : same as get tables + columns + count
privs                : get the privileges for the user
databases            : get the names of all the databases
EOF
		}
		case "brute" { print STDERR <<EOF;
Usage : brute tables / brute columns <table_name>

Note: If you are on MySQL >= 5, you probably want to use "get" instead.

Bruteforces the name of the tables, prefix with \$table_prefix. (or the name of the columns of the given table)
sqlsus will take its input from a dictionary, defined in \@brute_table_dict/\@brute_columns_dict, and depending on the configuration (\$uc_first and \$uc_all) will, for each item tried, try the uppercase and/or uppercase_first version of the word.
Following the dictionary attack, you will be asked if you want to go on with an exhaustive bruteforcer (chars a to z).
EOF
		}
		case "show" { print STDERR <<EOF;
Usage : show <item>

This command shows items already fetched via "get" or "start"

Possible items :
target               : show the target variables
privs                : show the privileges associated with the user
db [db_name]         : show the specified db structure (current if none specified)
databases            : show all the databases
all                  : same as show target + privs + databases
tables               : show the tables of the current database
columns <table_name> : show the columns for the specific table of the current database
EOF
		}
		case "set" { print STDERR <<EOF;
Usage : set [variable] [value]

This command sets (or print the current values of) variables.
"set" alone will print all the runtime variables/values
EOF
		}
		case "select" { print STDERR <<EOF;
Usage : select <something>

This command is intended to be used like a SELECT statement in MySQL
Note: you can use "select * from mytable", provided you have already "get" (or brute) the columns of the table.
EOF
		}
		case "test" { print STDERR <<EOF;
Usage : test <condition>

Limitations : blind mode only.

This command tests if the condition returns true or false.
EOF
		}
		case "find" { print STDERR <<EOF;
Usage : find <column_name>

Limitations : MySQL >= 5.

This command finds the tables that have a certain column. 
This is especially useful when in blind mode and you just want to retrieve the structure of some specific tables (ie: "find \%pass\%")
Note: SQL wildcards (\%_) can be used.
EOF
		}
		case "upload" { print STDERR <<EOF;
Usage : upload <local_file> <remote_filename>

Limitations : FILE privilege *and* the ability to inject quotes are needed.
              file size is limited to post_max_size (target php.ini)

If nothing has been done yet, depending on the configuration (upload_directories set or not), sqlsus will crawl the website looking for directories, and try to upoad the PHP uploader on each one (smartly).
If the tiny PHP uploader has been uploaded (via SELECT INTO OUTFILE), then sqlsus will upload the requested file via POST through it.
EOF
		}
		case "download" { print STDERR <<EOF;
Usage : download <remote_file>

Limitations : FILE privilege is needed.

This command outputs the content of the given file, and stores it locally to a local tree.
eg: download /etc/passwd will display the remote file /etc/passwd and store it to your local filesystem at $conf::datapath/$conf::filespath/etc/passwd

If you want to download a binary file (eg: /var/log/wtmp), "set binary 1" first.
EOF
		}
		case "eval" { print STDERR <<EOF;
Usage : eval <expression>

This command can be used to eval perl code from within sqlsus, allowing you to play with the internals of sqlsus.
EOF
		}
		case "!" { print STDERR <<EOF;
Usage : ! <command>

This command executes a command on the local system.
EOF
		}
		case "clone" { print STDERR <<EOF;
Usage : clone [table [col,col,..]]

This command clones the current database [or just a table [and just some columns]] to a local SQLite database.
Clonning can be resumed over sessions, it will start after the last fetched row, up to count(*).
EOF
		}
		case "use" { print STDERR <<EOF;
Usage : use <database>

Alias to "set database"
EOF
		}
		case "describe" { print STDERR <<EOF;
Usage : describe <table>

Alias to "show columns"
EOF
		}
		case "autoconf" { print STDERR <<EOF;
Usage : autoconf <item>

This command auto-configures some variables that are target dependant.

Possible items :
max_sendable    : maximum amount of data we can send at once to the target
select_columns  : columns to be used in inband mode
EOF
		}
		case "genconf" { print STDERR <<EOF;
Usage : genconf <filename>

This command generates a configuration file reflecting your current configuration.
EOF
		}
		case "replay" { print STDERR <<EOF;
Usage : replay

This command re-executes the last "select" in history even if a result for it exists in cache.
It will wipe the cached result, and print the "select" that is being replayed.
EOF
		}
		case "backdoor" { print STDERR <<EOF;
Usage : backdoor

Controls an already uploaded sqlsus backdoor (>= 0.5).
It uses the URL specified by the variable "backdoor".

If the variable "backdoor" is empty, it will try to upload the backdoor first.

Once inside the backdoor controller :
!<command>               : execute a command on the local system
\\<mode>                  : switch to the given MODE
\\upload <local> <remote> : upload a file
\\download <remote>       : download a file
\\help                    : print this help

Available MODEs :
exec : execute system commands on remote host
php  : eval() php code on remote host
sql <server> <user> <pass> <database> : inject sql on remote host

Use "\\exit" or Ctrl-D to exit the backdoor shell.
EOF
		}
		case "" { print STDERR <<EOF;
start    : where all the magic begins
get      : get information from system tables, such as the database(s) structure
brute    : bruteforce the names of tables or columns (when "get" is not possible)
show     : show information gathered with "start", "get" or "brute"
set      : view and set some variables
select   : inject a SELECT query
replay   : re-execute last select even if a result exists in cache
find     : find the tables where at least one column is like argument (you can use SQL wildcards)
upload   : upload a local file to the server, uploading the uploader first if needed
download : download a file via SELECT LOAD_FILE() and store the contents (and path tree) locally
autoconf : auto configure some variables
backdoor : upload (if needed) and control the sqlsus backdoor
clone    : clone the current database (see "use") [or just a table [and just some columns]] to a local db
genconf  : generate a configuration file with the current configuration
test     : inject a boolean query and return "Y" or "N" (blind mode only)
eval     : eval() perl code in sqlsus context
help     : diplay help regarding the command (ex: help get)
!        : execute a command on the local system

use      : alias to "set database", use it to change the current database
describe : alias to "show columns"

You can break a command using Ctrl-C
Use "help command" to get help on any of the above
EOF
		}
		else { print STDERR <<EOF;
No help available or command not implemented
EOF
		}
	}
	print STDERR "\n";
}

1
