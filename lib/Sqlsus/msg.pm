package msg;

# This file is part of sqlsus
#
# Copyright (c) 2008-2011 Jérémy Ruffet (sativouf)
# http://sqlsus.sourceforge.net/
#
# Licensed under GPLv3+: GNU GPL version 3 or later
# See LICENSE file or http://www.gnu.org/licenses/gpl.html

use strict;
use warnings;

##################################
sub error {
	my $msg = shift;
	print STDERR "[!] - ERROR - $msg\n" unless $main::internal_request;
}

##################################
sub warning {
	my $msg = shift;
	print STDERR "[!] - WARNING - $msg\n";
}

##################################
sub info {
	my $msg = shift;
	print STDERR "[+] $msg\n";
}

##################################
sub debug {
	my $msg = shift;
	print STDERR "[debug][$$] $msg\n" if $conf::debug;
}

##################################
sub debug2 {
	my $msg = shift;
	print STDERR "[debug2][$$] $msg\n" if $conf::debug == 2;
}
##################################
sub debug_raw {
	my $msg = shift;
	print STDERR "$msg" if $conf::debug;
}

##################################
sub notice {
	my $msg = shift;
	print STDERR "$msg\n";
}

##################################
sub raw {
	my $msg = shift;
	print STDERR "$msg";
}

##################################
sub log {
	my ($logfile, $msg) = @_;
	if (open LOG, ">>$logfile") {
		print LOG $msg;
		close LOG;
	} else {
		&msg::error("Could not open $logfile for appending : $!");
	}
}

##################################
sub fatal {
	my $msg = shift;
	print STDERR "[!] - FATAL - $msg\n";
	&main::save_and_exit(-1);
}

1

